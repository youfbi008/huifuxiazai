require('discourse/routes/topic').default.on("setupTopicController", function(event) {
  //LoginRequiredTopic = event.currentModel;
  TopicController = event.controller;
  TopicRoute = event;

  (function($) {

    // Applied to the reply-required class
    var applyLoginRequired = function($loginRequired, options) {
      var hidingElement, infoText,
        //isRepliedState = isLogined();

      hidingElement = $loginRequired;
      infoText = '登陆后可查看内容';

      if (Discourse.User.current()) {
        hidingElement.show(true);
      } else {
        hidingElement.show(false).replaceWith('<div class="login-required-info">' + infoText + '</div>');
      }
    };

    $.fn.loginRequired = function(options) {
      var opts = options || {},
        replies = this.each(function() {
          applyLoginRequired($(this), opts);
        });

      return replies;
    };

  })(jQuery);
});
